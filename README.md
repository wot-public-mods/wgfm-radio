﻿**NetRadioWOT Radio** This is a modification for the game "World Of Tanks" which allows you to listen NetRadioWOT radio directly ingame

### An example of what radio looks like
![An example of what radio looks like](https://gitlab.com/wot-public-mods/wgfm-radio/-/raw/master/ui_preview.png)